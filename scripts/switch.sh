#!/usr/bin/env bash

PROTOCOL=$1
DOMAIN=$2

# 현재 stop.sh가 속해 있는 경로를 찾음
ABSPATH=$(readlink -f "$0")
ABSDIR=$(dirname "$ABSPATH")
# shellcheck disable=SC1090
# 자바로 보면 일종의 import 구문
# 해당 코드로 인해 stop.sh에서도 profile.sh의 여러 function을 사용할 수 있음.
source "${ABSDIR}"/profile.sh "$PROTOCOL" "$DOMAIN"

function switch_proxy() {
  IDLE_PORT=$(find_idle_port)

  echo "> 전환할 Port: $IDLE_PORT"
  echo "> Port 전환"
  # 하나의 문장(echo구문)을 파이프라인(|)으로 넘겨주기 위해 echo를 사용
  # tee: 터미널 출력을 파일로 저장
  echo "set \$service_url 127.0.0.1:${IDLE_PORT};" | tee /etc/nginx/conf.d/service-url.inc
  sleep 5

  echo "> Nginx Reload"
  # Nginx 설정을 다시 불러옴
  # reload는 끊김 없이 다시 불러옴
  # 다만, 중요한 설정들은 반영되지 않으므로 restart를 사용해야함.
  # 여기선 외부의 설정 파일인 service-url을 다시 불러오는 거라 reload로 가능
  # TODO: Vault를 이용하여 password 암호화하여 저장하고 관리하는게 필요 => jenkins <=> Vault 연결하여 script 구성이 목표.
  echo -e 'msl1234~' | sudo -S service nginx reload
  sleep 5
}

function shutdown_previous_version_server() {
  IDLE_PROFILE=$(find_idle_profile)

  echo "> Graceful ShutDown Profile: $IDLE_PROFILE"

  # 서버에 첫 배포를 시도할때, 이전 버전의 application은 실행중이지 않으므로 handling
  # shellcheck disable=SC2034
  RESPONSE=$(curl -s http://localhost:"${IDLE_PORT}"/gateway/profile)
  if [ -n "${RESPONSE}" ]
  then
    echo "> 서버에 첫 배포를 시도하는 케이스라 이전 버전의 application은 존재하지 않습니다."
    exit 1
  fi

  echo "Stop maum-api-gate-$IDLE_PROFILE"
  docker stop maum-api-gate-"$IDLE_PROFILE"
  sleep 5
}