#!/usr/bin/env bash

PROTOCOL=$1
DOMAIN=$2

# 현재 stop.sh가 속해 있는 경로를 찾음
ABSPATH=$(readlink -f "$0")
ABSDIR=$(dirname "$ABSPATH")

# shellcheck disable=SC1090
# 자바로 보면 일종의 import 구문
# 해당 코드로 인해 stop.sh에서도 profile.sh의 여러 function을 사용할 수 있음.
source "${ABSDIR}"/profile.sh "$PROTOCOL" "$DOMAIN"

IDLE_PROFILE=$(find_idle_profile)
IDLE_PORT=$(find_idle_port)

MINDS_DOCKER_REGISTRY=$3
LOG_VOLUME=$4
GIT_TAG=$5
SPRING_PROFILES_ACTIVE=$6

echo "> profile=$IDLE_PROFILE 로 docker 실행"
echo "> docker run -d -p $IDLE_PORT:7000 -e PROFILE=$IDLE_PROFILE -e SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v $LOG_VOLUME --name maum-api-gate-$IDLE_PROFILE ${MINDS_DOCKER_REGISTRY}/maum-api-gate:${GIT_TAG}"
docker run -d -p "$IDLE_PORT":7000 -e PROFILE="$IDLE_PROFILE" -e SPRING_PROFILES_ACTIVE="$SPRING_PROFILES_ACTIVE" -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v "$LOG_VOLUME" --name maum-api-gate-"$IDLE_PROFILE" "${MINDS_DOCKER_REGISTRY}"/maum-api-gate:"${GIT_TAG}"

echo "> profile=backup 으로 docker 실행"
echo "> docker run -d -p 7002:7000 -e PROFILE=backup -e SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v $LOG_VOLUME --name media-server-backup ${MINDS_DOCKER_REGISTRY}/maum-api-gate:${GIT_COMMIT}"
docker run -d -p 7002:7000 -e PROFILE=backup -e SPRING_PROFILES_ACTIVE="$SPRING_PROFILES_ACTIVE" -v /etc/localtime:/etc/localtime:ro -v /usr/share/zoneinfo/Asia/Seoul:/etc/timezone:ro -v "$LOG_VOLUME" --name media-server-backup "${MINDS_DOCKER_REGISTRY}"/maum-api-gate:"${GIT_COMMIT}"