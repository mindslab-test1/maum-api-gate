#!/usr/bin/env bash

# 현재 stop.sh가 속해 있는 경로를 찾음
ABSPATH=$(readlink -f "$0")
ABSDIR=$(dirname "$ABSPATH")

PROTOCOL=$1
DOMAIN=$2

# shellcheck disable=SC1090
# 자바로 보면 일종의 import 구문
# 해당 코드로 인해 stop.sh에서도 profile.sh의 여러 function을 사용할 수 있음.
source "${ABSDIR}"/profile.sh "$PROTOCOL" "$DOMAIN"

IDLE_PORT=$(find_idle_port)
IDLE_PROFILE=$(find_idle_profile)

if [ "${IDLE_PROFILE}" == backup ]
then
  echo "> 현재 구동 중인 Docker Container가 알 수 없는 이유로 죽게 되어 backup Container가 작동 중입니다."
  echo "> Nginx가 backup Container를 바라보는 구조에선 현재 무중단 배포를 이용할 수 없습니다."
  echo "exit 1"
  exit 1
else
  echo "> $IDLE_PROFILE docker container id 확인"
  IDLE_CONTAINER_ID=$(docker ps -aqf "name=maum-api-gate-$IDLE_PROFILE")

  echo "> backup docker container id 확인"
  BACKUP_CONTAINER_ID=$(docker ps -aqf "name=maum-api-gate-backup")
fi

if [ -z "${IDLE_CONTAINER_ID}" ] && [ -z "${BACKUP_CONTAINER_ID}" ]
then
  echo "> 현재 구동 중인 Docker Container가 없으므로 종료하지 않습니다."
else
  echo "> docker rm -f $IDLE_CONTAINER_ID"
  docker rm -f "${IDLE_CONTAINER_ID}"
  echo "> docker rm -f $BACKUP_CONTAINER_ID"
  docker rm -f "${BACKUP_CONTAINER_ID}"
  sleep 50
fi
