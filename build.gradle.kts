import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.*

buildscript {
	extra["springBootVersion"] = "2.3.3.RELEASE"
	extra["kotlinVersion"] = "1.3.72"

	repositories {
		mavenCentral()
	}

	dependencies {
		classpath("com.google.protobuf:protobuf-gradle-plugin:0.8.10")
		classpath("org.springframework.boot:spring-boot-gradle-plugin:${project.extra["springBootVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:${project.extra["kotlinVersion"]}")
		classpath("org.jetbrains.kotlin:kotlin-allopen:${project.extra["kotlinVersion"]}")
	}
}

plugins {
	id("org.springframework.boot") version "2.3.3.RELEASE"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	id("com.google.protobuf") version "0.8.13"
	kotlin("jvm") version "1.3.72"
	kotlin("plugin.spring") version "1.3.72"

	// It is a necessary plug-in for an optimized Docker image configuration.
	// Reference Link: https://github.com/palantir/gradle-docker
	id("com.palantir.docker") version "0.22.1"
}

group = "ai.maum.api"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

val grpcVersion = "1.30.0"
val protocVersion = "3.13.0"

repositories {
	mavenCentral()
}

sourceSets {
	main {
		java {
			srcDir("src/main/protoGen")
		}
	}
}

extra["springCloudVersion"] = "Hoxton.SR7"

dependencies {
	implementation("org.springframework.cloud:spring-cloud-starter-gateway")

	// grpc
	implementation("io.grpc:grpc-services")
	implementation("io.grpc:grpc-netty")
	implementation("io.github.lognet:grpc-spring-boot-starter:3.5.5")

	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	// logging
	implementation("org.springframework.boot:spring-boot-starter-log4j2")
	implementation("com.lmax:disruptor:3.4.2")

	implementation("org.apache.httpcomponents:httpclient:4.5.13")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
}

configurations {
	all {
		exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
	}
}

dependencyManagement {
	imports {
		mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
	}
}

protobuf {
	protoc { artifact = "com.google.protobuf:protoc:$protocVersion" }
	plugins {
		id("grpc") { artifact = "io.grpc:protoc-gen-grpc-java:$grpcVersion" }
	}
	generateProtoTasks {
		ofSourceSet("main").forEach { task ->
//            task.builtins {
//                id("java") {
//                    outputSubDir = "protoGen"
//                }
//            }
			task.plugins {
				id("grpc") {
					outputSubDir = "protoGen"
				}
			}
		}
	}
	generatedFilesBaseDir = "$projectDir/src/"
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.create<Copy>("unpack") {
	dependsOn(tasks.bootJar)
	from(zipTree(tasks.bootJar.get().outputs.files.singleFile))
	into("build/dependency")
}

docker {
	name = "maum-api-gate"
	copySpec.from(tasks.findByName("unpack")?.outputs).into("dependency")
	buildArgs(mapOf("DEPENDENCY" to "dependency"))
}
