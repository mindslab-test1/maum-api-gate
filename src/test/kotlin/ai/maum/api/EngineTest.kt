package ai.maum.api

import ai.maum.api.core.model.Engine
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.http.server.RequestPath
import java.net.URI

@DisplayName("Engine Model Unit Test")
class EngineTest {

    @Test
    @DisplayName("RequestPath에 대한 Engine Name 찾는 테스트")
    fun testFindByRequestPath() {
        // given
        val mockRequestPath = RequestPath.parse(URI.create("http://localhost:8080/stt/cnn"), "")
        println(mockRequestPath.toString())

        // when
        val result = Engine.findByRequestPath(mockRequestPath)
        println(result)

        // then
        assertThat(result).isEqualTo(Engine.STT.value)
    }

    @Test
    @DisplayName("RequestPath에 대한 Engine Name이 없는 경우의 테스트")
    fun testFindByRequestPathDefault() {
        // given
        val mockValue = "abcd"
        val expectedValue = mockValue
        val mockRequestPath = RequestPath.parse(URI.create("http://localhost:8080/$mockValue/cnn"), "")
        println(mockRequestPath.toString())

        // when
        val result = Engine.findByRequestPath(mockRequestPath)
        println(result)

        // then
        assertThat(result).isEqualTo(expectedValue)
    }
}