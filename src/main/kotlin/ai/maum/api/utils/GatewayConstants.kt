package ai.maum.api.utils

object GatewayConstants {
    // Custom Http Headers
    const val REQUEST_TIME = "Request-Time"
    const val REQUEST_AUTHORIZATION = "Request-Authorization"
    const val REQUEST_UNIQUE_KEY = "Request-Unique-Key"
    const val REQUEST_USAGE = "Request-Usage"
    const val RESPONSE_USAGE = "Response-Usage"

    const val USER_AGENT = "User-Agent"

    const val BASIC: String = "Basic"
    const val BEARER: String = "Bearer"
}
