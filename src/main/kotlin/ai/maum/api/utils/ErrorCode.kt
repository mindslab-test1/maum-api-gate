package ai.maum.api.utils

object ErrorCode {
    const val UNAUTHORIZED = "unauthorized"
    const val ACCESS_DENIED = "access_denied"
    const val FAILED_TO_GET_ACCESS_TOKEN = "failed_to_get_access_token"
    const val INVALID_AUTH_TYPE = "invalid_auth_type"
    const val INVALID_AUTH_VALUE = "invalid_auth_value"
    const val NOT_FOUND_AUTH_HEADER = "not_found_auth_header"
}
