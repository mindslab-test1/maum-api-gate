package ai.maum.api.utils

import ai.maum.api.core.exception.InvalidAuthTypeException
import ai.maum.api.core.exception.InvalidAuthValueException
import ai.maum.api.core.exception.NotFoundAuthHeaderException
import org.springframework.http.HttpHeaders
import java.util.Base64

object AuthorizationParser {

    fun parseBasicValue(httpHeaders: HttpHeaders) : String {
        val authorizationValues = getAuthorizationValues(httpHeaders)
        return validateBasicValue(authorizationValues)
    }

    private fun getAuthorizationValues(httpHeaders: HttpHeaders) : List<String> {
        val authorizationValue = getAuthorizationValue(httpHeaders)
        return splitAuthorizationValue(authorizationValue)
    }

    private fun validateBasicValue(authorizationValues: List<String>): String {
        if (authorizationValues[0] != GatewayConstants.BASIC) {
            throw InvalidAuthTypeException("Invalid Authorization type. [${GatewayConstants.BASIC}, ${GatewayConstants.BEARER}]")
        }
        val authorizationValue = authorizationValues[1]
        // Base64 validation
        // 'IDo=', 'IDog' -> space
        // 'Og==' -> empty or null
        if (authorizationValue == "Og==") {
            throw InvalidAuthValueException("Invalid Authorization value. Empty or Null.")
        }
        if (authorizationValue == "IDo=" || authorizationValue == "IDog") {
            throw InvalidAuthValueException("Invalid Authorization value. Contains space username or password.")
        }

        return authorizationValue
    }

    private fun getAuthorizationValue(httpHeaders: HttpHeaders) : String {
        return when {
            httpHeaders.containsKey(GatewayConstants.REQUEST_AUTHORIZATION) -> {
                httpHeaders.getValue(GatewayConstants.REQUEST_AUTHORIZATION).first()
            }
            httpHeaders.containsKey(HttpHeaders.AUTHORIZATION) -> {
                httpHeaders.getValue(HttpHeaders.AUTHORIZATION).first()
            }
            else -> {
                throw NotFoundAuthHeaderException("Not Found Header Authorization [${GatewayConstants.REQUEST_AUTHORIZATION}, ${HttpHeaders.AUTHORIZATION}]")
            }
        }
    }

    private fun splitAuthorizationValue(authorizationValueString: String) : List<String> {
        return if (authorizationValueString.contains(" ")) {
            authorizationValueString.split(" ")
        } else {
            listOf(authorizationValueString)
        }
    }

    fun parseDecodedValue(httpHeaders: HttpHeaders) : Pair<String, String> {
        val authorizationValues = getAuthorizationValues(httpHeaders)
        val authorizationValue = if (authorizationValues.size == 1) {
            authorizationValues[0]
        } else {
            validateBasicValue(authorizationValues)
        }

        val decoder = Base64.getDecoder()
        val decodedValues = String(decoder.decode(authorizationValue)).split(":")
        return Pair(decodedValues[0], decodedValues[1])
    }
}
