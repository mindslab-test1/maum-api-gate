package ai.maum.api.infra

import ai.maum.api.core.exception.BaseException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.http.client.ClientHttpResponse
import org.springframework.web.client.ResponseErrorHandler
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.nio.charset.StandardCharsets


class RestTemplateErrorHandler : ResponseErrorHandler {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun hasError(response: ClientHttpResponse): Boolean {
        return !response.statusCode.is2xxSuccessful
    }

    override fun handleError(response: ClientHttpResponse) {
        val error = getErrorAsString(response)
        logger.error("""
            
            ==============================[Response]==============================
            Headers: ${response.headers}
            StatusCode: ${response.rawStatusCode}
            Body: $error
        """.trimIndent())
        val errorMap = ObjectMapper().readValue(error, object : TypeReference<Map<String, String>>() {})
        val code = errorMap["code"] ?: "internal_server_error"
        val message = errorMap["message"] ?: "Internal Server Error"
        throw BaseException(code, message)
    }

    private fun getErrorAsString(response: ClientHttpResponse): String {
        return try {
            val outputStream = ByteArrayOutputStream()
            response.body.use { input ->
                outputStream.use { output ->
                    input.copyTo(output)
                }
            }
            val byteArray = outputStream.toByteArray()
            return String(byteArray, StandardCharsets.UTF_8)
        } catch (e: IOException) {
            e.localizedMessage
        }
    }
}
