package ai.maum.api.infra.config

import ai.maum.api.infra.JsonExceptionHandler
import org.springframework.beans.factory.ObjectProvider
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.codec.ServerCodecConfigurer
import org.springframework.web.reactive.result.view.ViewResolver
import java.util.*


@Configuration
class ExceptionConfig {

    @Primary
    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE)
    fun errorWebExceptionHandler(viewResolversProvider: ObjectProvider<List<ViewResolver?>?>,
                                 serverCodecConfigurer: ServerCodecConfigurer): ErrorWebExceptionHandler? {
        val jsonExceptionHandler = JsonExceptionHandler()
        jsonExceptionHandler.setViewResolvers(viewResolversProvider.getIfAvailable(Collections::emptyList))
        jsonExceptionHandler.setMessageWriters(serverCodecConfigurer.writers)
        jsonExceptionHandler.setMessageReaders(serverCodecConfigurer.readers)
        return jsonExceptionHandler
    }
}