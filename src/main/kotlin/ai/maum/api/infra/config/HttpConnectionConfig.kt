package ai.maum.api.infra.config

import ai.maum.api.infra.RestTemplateClientHttpRequestInterceptor
import ai.maum.api.infra.RestTemplateErrorHandler
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import org.springframework.web.reactive.function.client.WebClient
import java.time.Duration

@Configuration
class HttpConnectionConfig {

    @Bean
    fun webClient(): WebClient {
        return WebClient.create()
    }

    @Bean
    fun restTemplate(): RestTemplate {
        return RestTemplateBuilder()
                // 로깅 인터셉터에서 Stream을 소비하므로 BufferingClientHttpRequestFactory 을 꼭 써야한다.
                .requestFactory { BufferingClientHttpRequestFactory(HttpComponentsClientHttpRequestFactory()) }
                .errorHandler(RestTemplateErrorHandler())
                .setConnectTimeout(Duration.ofMinutes(1))
                .additionalInterceptors(RestTemplateClientHttpRequestInterceptor())
                .build()
    }
}
