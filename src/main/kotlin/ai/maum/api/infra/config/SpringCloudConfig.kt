package ai.maum.api.infra.config

import org.slf4j.LoggerFactory
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.cloud.gateway.filter.GlobalFilter
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.GatewayFilterSpec
import org.springframework.cloud.gateway.route.builder.PredicateSpec
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Configuration
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Configuration
class SpringCloudConfig {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    // Routing based java bean config
//    @Bean
    fun gatewayRoutes(builder: RouteLocatorBuilder): RouteLocator? {
        return builder.routes()
            .route { r: PredicateSpec ->
                r
                    .path("/tts/**")
                    // pre and post filters provided by spring cloud gateway
                    .filters { f: GatewayFilterSpec ->
                        f
                            .addRequestHeader("tts-request", "tts-request-header")
                            .addResponseHeader("tts-response", "tts-response-header")
                    }
                    .uri("http://localhost:8081/")
                    .id("TTS-Service")
            }
            .route { r: PredicateSpec ->
                r
                    .path("/stt/**")
                    // pre and post filters provided by spring cloud gateway
                    .filters { f: GatewayFilterSpec ->
                        f
                            .addRequestHeader("stt-request", "stt-request-header")
                            .addResponseHeader("stt-response", "stt-response-header")
                    }
                    .uri("http://localhost:8082/")
                    .id("STT-Service")
            }
            .route { r: PredicateSpec ->
                r
                    .path("/lipsync/**")
                    .filters { f: GatewayFilterSpec ->
                        f
                            .addRequestHeader("lipsync-request", "lipsync-request-header")
                            .addResponseHeader("lipsync-response", "lipsync-response-header")
                    }
                    .uri("http://localhost:8083/")
                    .id("Lipsync-Service")
            }
            .build()
    }

//    @Bean
    fun globalFilter(): GlobalFilter? {
        return GlobalFilter { exchange: ServerWebExchange?, chain: GatewayFilterChain ->
            logger.info("Pre Global filter")
            chain.filter(exchange).then(Mono.fromRunnable {
                logger.info("Post Global filter")
            })
        }
    }
}
