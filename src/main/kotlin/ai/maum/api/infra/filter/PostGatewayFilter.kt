package ai.maum.api.infra.filter

import ai.maum.api.core.model.Engine
import ai.maum.api.infra.service.stats.ApiUsageSaveDto
import ai.maum.api.infra.service.stats.StatsService
import ai.maum.api.utils.AuthorizationParser
import ai.maum.api.utils.GatewayConstants
import org.slf4j.LoggerFactory
import org.springframework.cloud.gateway.filter.GatewayFilter
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.time.Instant


@Component
class PostGatewayFilter (
    private val statsService: StatsService
) : GatewayFilterFactory<PostGatewayFilter.Config> {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun apply(config: Config): GatewayFilter {
        return GatewayFilter { exchange: ServerWebExchange, chain: GatewayFilterChain ->
            return@GatewayFilter chain.filter(exchange).then(Mono.fromRunnable {
                logger.info("Access post filter about '${exchange.request.path}'")

                // request
                val request = exchange.request
                val requestHeaders = request.headers

                // TODO: user request user-agent 및 client-hints 대응
                val requestUserAgent = requestHeaders.getFirst(GatewayConstants.USER_AGENT)
                var requestAosPackage = ""
                var requestIosUrlScheme = ""
                requestUserAgent?.let {
                    if (it.contains("Android")) {
                        requestAosPackage = "Android"
                    }
                }
                requestUserAgent?.let {
                    if (it.contains("iPhone")) {
                        requestIosUrlScheme = "iPhone"
                    }
                }
                val requestTimeMillis = requestHeaders.getFirst(GatewayConstants.REQUEST_TIME)?.toLong() ?: 0L

                val requestPath = request.path // ex> /stt/*, /tts/*, /lipsync/*

                val requestUniqueKey = requestHeaders.getFirst(GatewayConstants.REQUEST_UNIQUE_KEY) ?: ""

                // response
                val response = exchange.response
                val responseHeaders = response.headers

                val responseTime = Instant.now()

                val requestUsage = responseHeaders.getFirst(GatewayConstants.REQUEST_USAGE)?.toLong() ?: 0L
                logger.info("RequestUsage: $requestUsage")

                val responseUsage = responseHeaders.getFirst(GatewayConstants.RESPONSE_USAGE)?.toLong() ?: 0L
                logger.info("ResponseUsage: $responseUsage")

                // async grpc call
                statsService.saveApiUsage(ApiUsageSaveDto(
                        apiId = AuthorizationParser.parseDecodedValue(requestHeaders).first,
                        elapsedTime = responseTime.minusMillis(requestTimeMillis).toEpochMilli(),
                        engine = Engine.findByRequestPath(requestPath),
                        service = "",
                        pathAbsolute = requestPath.toString(),
                        requestAosPackage = requestAosPackage,
                        requestIosUrlScheme = requestIosUrlScheme,
                        requestApplicationName = requestUserAgent ?: "",
                        requestContentType = requestHeaders.contentType?.toString() ?: MediaType.APPLICATION_JSON_VALUE,
                        requestHost = requestHeaders.host?.hostString ?: "",
                        requestMethod = request.methodValue,
                        requestPayloadSize = requestUsage,
                        requestTime = requestTimeMillis,
                        responseContentType = responseHeaders.contentType?.toString()
                                ?: MediaType.APPLICATION_JSON_VALUE,
                        responsePayloadSize = responseUsage,
                        responseStatusCode = response.rawStatusCode?.toLong() ?: 0L,
                        responseTime = responseTime.toEpochMilli(),
                        success = response.statusCode?.is2xxSuccessful ?: false,
                        uuid = requestUniqueKey
                ))

            })
        }
    }

    override fun getConfigClass(): Class<Config> {
        return Config::class.java
    }

    class Config(var name: String = "temp")
}
