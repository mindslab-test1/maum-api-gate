package ai.maum.api.infra.filter

import ai.maum.api.core.exception.FailedToGetAccessTokenException
import ai.maum.api.core.exception.NotFoundAuthHeaderException
import ai.maum.api.infra.service.auth.AuthService
import ai.maum.api.utils.AuthorizationParser
import ai.maum.api.utils.GatewayConstants
import org.slf4j.LoggerFactory
import org.springframework.cloud.gateway.filter.GatewayFilter
import org.springframework.cloud.gateway.filter.GatewayFilterChain
import org.springframework.cloud.gateway.filter.factory.GatewayFilterFactory
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import java.time.Instant
import java.util.*

@Component
class PreGatewayFilter (
    private val authService: AuthService
) : GatewayFilterFactory<PreGatewayFilter.Config> {

    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun apply(config: Config): GatewayFilter {
        // grab configuration from Config object
        return GatewayFilter { exchange: ServerWebExchange, chain: GatewayFilterChain ->
            //If you want to build a "pre" filter you need to manipulate the
            //request before calling chain.filter
            logger.info("Access pre filter about '${exchange.request.path}'")

            val serverHttpRequest = exchange.request
            val httpHeaders = serverHttpRequest.headers
            if (!httpHeaders.contains(HttpHeaders.AUTHORIZATION)) {
                throw NotFoundAuthHeaderException("Not found Authorization in HttpHeaders")
            }

            val requestTime = Instant.now().toEpochMilli().toString()
            val requestUniqueKey = UUID.randomUUID()

            val authorizationValue = AuthorizationParser.parseBasicValue(httpHeaders)
            val authResponse = authService.findAccessTokenByBasicToken(authorizationValue) ?: throw FailedToGetAccessTokenException("Failed to get access-token")

            val serverHttpRequestWithBearerAuthHeader = serverHttpRequest.mutate()
                .header(GatewayConstants.REQUEST_TIME, requestTime)
                .header(GatewayConstants.REQUEST_UNIQUE_KEY, requestUniqueKey.toString())
                .header(GatewayConstants.REQUEST_AUTHORIZATION, authorizationValue)
                .header(HttpHeaders.AUTHORIZATION, "${GatewayConstants.BEARER} ${authResponse.accessToken}")
                .build()

            chain.filter(exchange.mutate().request(serverHttpRequestWithBearerAuthHeader).build())
        }
    }

    override fun getConfigClass(): Class<Config> {
        return Config::class.java
    }

    class Config(var name: String = "temp")
}
