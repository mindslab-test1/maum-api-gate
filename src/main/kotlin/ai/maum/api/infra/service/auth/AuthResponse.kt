package ai.maum.api.infra.service.auth

import com.fasterxml.jackson.annotation.JsonProperty

//{
//    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJzdHQiLCJ0dHMiLCJhdmF0YXIiXSwiZXhwIjoxNTk5MTc4MTcwLCJhdXRob3JpdGllcyI6WyJST0xFX0FQSSIsIlJPTEVfQU1MIiwiUk9MRV9TVEFUUyJdLCJqdGkiOiJiNWJiZDBlOC05NjE0LTRiNTgtOGNmNi1iYTU2OTQ1MjA4NWYiLCJjbGllbnRfaWQiOiJhMTUxODdmNS1hNzk3LTRjMDAtYTY2ZC03NmY4MTk1YzNlZTUifQ.n-tf0T6-PxwNfKPL7nM58Q1XqPQ7W3fKFbQRCaP6ick",
//    "token_type": "bearer",
//    "expires_in": 1209599,
//    "scope": "stt tts avatar",
//    "jti": "b5bbd0e8-9614-4b58-8cf6-ba569452085f"
//}
data class AuthResponse (
    @JsonProperty("access_token")
    val accessToken: String,
    @JsonProperty("token_type")
    val tokenType: String,
    @JsonProperty("expires_in")
    val expiresIn: Long,
    @JsonProperty("scope")
    val scope: String,
    @JsonProperty("jti")
    val jti: String
)
