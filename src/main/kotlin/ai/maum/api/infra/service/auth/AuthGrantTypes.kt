package ai.maum.api.infra.service.auth

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue

enum class AuthGrantTypes(@get:JsonValue val value: String) {
    CLIENT_CREDENTIALS("client_credentials"),
    REFRESH_TOKEN("refresh_token"),
    PASSWORD("password"),
    AUTHORIZATION_CODE("authorization_code"),
    IMPLICIT("implicit");

    companion object {
        private val map = values().associateBy(AuthGrantTypes::value)

        @JsonCreator
        @JvmStatic
        fun fromValue(value: String) = map.getOrDefault(value, CLIENT_CREDENTIALS)
    }
}
