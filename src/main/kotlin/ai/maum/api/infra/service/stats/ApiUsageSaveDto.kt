package ai.maum.api.infra.service.stats

data class ApiUsageSaveDto(
    val apiId: String,
    val elapsedTime: Long,
    val engine: String,
    val service: String,
    val requestAosPackage: String,
    val requestIosUrlScheme: String,
    val requestApplicationName: String,
    val requestHost: String,
    val requestMethod: String,
    val requestPayloadSize: Long,
    val requestTime: Long,
    val requestContentType: String,
    val pathAbsolute: String,
    val responsePayloadSize: Long,
    val responseStatusCode: Long,
    val responseTime: Long,
    val responseContentType: String,
    val success: Boolean,
    val uuid: String
)
