package ai.maum.api.infra.service.stats


import ai.maum.api.infra.property.ExternalURIProperties
import ai.maum.stats.protobuf.SaveAPIUsageRequest
import ai.maum.stats.protobuf.SaveAPIUsageResponse
import ai.maum.stats.protobuf.StatsGrpcServiceGrpc
import io.grpc.ManagedChannelBuilder
import io.grpc.stub.StreamObserver
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

fun ApiUsageSaveDto.toSaveApiUsageRequestBuilder(): SaveAPIUsageRequest.Builder {
    return SaveAPIUsageRequest.newBuilder()
        .setApiId(this.apiId)
        .setElapsedTime(this.elapsedTime)
        .setEngine(this.engine)
        .setService(this.service)
        .setRequestAosPackage(this.requestAosPackage)
        .setRequestIosUrlScheme(this.requestIosUrlScheme)
        .setRequestApplicationName(this.requestApplicationName)
        .setRequestHost(this.requestHost)
        .setRequestMethod(this.requestMethod)
        .setRequestPayloadSize(this.requestPayloadSize)
        .setRequestTime(this.requestTime)
        .setRequestContentType(this.requestContentType)
        .setPathAbsolute(this.pathAbsolute)
        .setResponsePayloadSize(this.responsePayloadSize)
        .setResponseStatusCode(this.responseStatusCode)
        .setResponseTime(this.responseTime)
        .setResponseContentType(this.responseContentType)
        .setSuccess(this.success)
        .setUuid(this.uuid)
}

@Service
class StatsServiceImpl(
    externalURIProperties: ExternalURIProperties
) : StatsService {
    private val statsGrpcChannel = ManagedChannelBuilder.forTarget(externalURIProperties.statsServerUri).usePlaintext().build()
    private val statsStub = StatsGrpcServiceGrpc.newStub(statsGrpcChannel)
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun saveApiUsage(apiUsageSaveDto: ApiUsageSaveDto) {
        val saveApiUsageRequestBuilder = apiUsageSaveDto.toSaveApiUsageRequestBuilder()
        logger.info("Call stats producer server (based on gRPC)")
        statsStub.saveApiUsage(saveApiUsageRequestBuilder.build(), object : StreamObserver<SaveAPIUsageResponse> {
            override fun onNext(value: SaveAPIUsageResponse?) {
                logger.info(value?.message)
            }

            override fun onError(t: Throwable?) {
                logger.error(t.toString())
            }

            override fun onCompleted() {}
        })
    }
}
