package ai.maum.api.infra.service.auth

import ai.maum.api.infra.property.ExternalURIProperties
import ai.maum.api.utils.GatewayConstants
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.reactive.function.client.WebClient
import java.net.URI

@Service
class AuthService(
    private val webClient: WebClient,
    private val restTemplate: RestTemplate,
    private val externalURIProperties: ExternalURIProperties
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun findAccessTokenByBasicToken(basicToken: String): AuthResponse? {
        // async callback이면 안됨. auth는 blocking sync
//        return webClient.mutate()
//                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
//                .defaultHeader(HttpHeaders.AUTHORIZATION, "${GatewayConstants.BASIC} $basicToken").build()
//                .post()
//                .uri("http://localhost:8080/oauth/token")
//                .body(BodyInserters.fromFormData("grant_type", AuthGrantTypes.CLIENT_CREDENTIALS.value))
//                .retrieve()
//                .bodyToMono<AuthResponse>(AuthResponse::class.java).block()
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_FORM_URLENCODED
        httpHeaders.set(HttpHeaders.AUTHORIZATION, "${GatewayConstants.BASIC} $basicToken")

        val multiValueMap = LinkedMultiValueMap<String, String>()
        multiValueMap["grant_type"] = AuthGrantTypes.CLIENT_CREDENTIALS.value

        val httpEntity = HttpEntity<MultiValueMap<String, String>>(multiValueMap, httpHeaders)

        val responseEntity = restTemplate.exchange(URI.create("${externalURIProperties.authServerUri}/auth/oauth/token"), HttpMethod.POST, httpEntity, AuthResponse::class.java)

        return responseEntity.body
    }
}
