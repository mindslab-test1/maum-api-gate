package ai.maum.api.infra.service.stats

interface StatsService {
    fun saveApiUsage(apiUsageSaveDto: ApiUsageSaveDto)
}
