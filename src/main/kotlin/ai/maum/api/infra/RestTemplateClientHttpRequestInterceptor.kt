package ai.maum.api.infra

import org.slf4j.LoggerFactory
import org.springframework.http.HttpRequest
import org.springframework.http.client.ClientHttpRequestExecution
import org.springframework.http.client.ClientHttpRequestInterceptor
import org.springframework.http.client.ClientHttpResponse
import java.io.ByteArrayOutputStream
import java.nio.charset.StandardCharsets


class RestTemplateClientHttpRequestInterceptor : ClientHttpRequestInterceptor {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    override fun intercept(request: HttpRequest, body: ByteArray, execution: ClientHttpRequestExecution): ClientHttpResponse {
        loggingRequest(request, body)
        val response = execution.execute(request, body)

        loggingResponse(response)
        return execution.execute(request, body)
    }

    private fun loggingRequest(request: HttpRequest, body: ByteArray) {
        logger.info("""
            
            ==============================[Request]==============================
            Headers: ${request.headers}
            Method: ${request.method}
            URI: ${request.uri}
            Body: ${if (body.isEmpty()) "" else String(body, StandardCharsets.UTF_8)}
        """.trimIndent())
    }

    private fun loggingResponse(response: ClientHttpResponse) {
        if (!response.statusCode.is2xxSuccessful) {
            return
        }
        val body = getBody(response)
        logger.info("""
            
            ==============================[Response]==============================
            Headers: ${response.headers}
            StatusCode: ${response.rawStatusCode}
            Body: $body
        """.trimIndent())
    }

    private fun getBody(response: ClientHttpResponse): String {
        val outputStream = ByteArrayOutputStream()
        response.body.use { input ->
            outputStream.use { output ->
                input.copyTo(output)
            }
        }
        val byteArray = outputStream.toByteArray()
        return String(byteArray, StandardCharsets.UTF_8)
    }

}