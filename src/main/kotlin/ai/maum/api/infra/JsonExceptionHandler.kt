package ai.maum.api.infra

import ai.maum.api.core.exception.BaseException
import ai.maum.api.core.exception.ClientAccessDeniedException
import ai.maum.api.core.exception.ClientUnAuthorizedException
import ai.maum.api.utils.ErrorCode
import org.slf4j.LoggerFactory
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.codec.HttpMessageReader
import org.springframework.http.codec.HttpMessageWriter
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.*
import org.springframework.web.reactive.result.view.ViewResolver
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import java.util.*

class JsonExceptionHandler : ErrorWebExceptionHandler {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    /**
     * MessageReader
     */
    private var messageReaders: List<HttpMessageReader<*>> = emptyList()

    /**
     * MessageWriter
     */
    private var messageWriters: List<HttpMessageWriter<*>> = emptyList()

    /**
     * ViewResolvers
     */
    private var viewResolvers: List<ViewResolver?> = emptyList()

    /**
     * Store information after processing exceptions
     */
    private val exceptionHandlerResult = ThreadLocal<Map<String, Any>>()

    /**
     * Reference AbstractErrorWebExceptionHandler
     */
    fun setMessageReaders(messageReaders: List<HttpMessageReader<*>>) {
        this.messageReaders = messageReaders
    }

    /**
     * Reference AbstractErrorWebExceptionHandler
     */
    fun setViewResolvers(viewResolvers: List<ViewResolver?>) {
        this.viewResolvers = viewResolvers
    }

    /**
     * Reference AbstractErrorWebExceptionHandler
     */
    fun setMessageWriters(messageWriters: List<HttpMessageWriter<*>>) {
        this.messageWriters = messageWriters
    }

    override fun handle(exchange: ServerWebExchange, ex: Throwable): Mono<Void> {
        // According to the type of exception processing
        val httpStatus: HttpStatus
        var code = if (ex is BaseException) {
            ex.code
        } else {
            ""
        }
        val message: String

        when {
            (ex is ClientUnAuthorizedException || code == ErrorCode.UNAUTHORIZED) -> {
                httpStatus = HttpStatus.UNAUTHORIZED
                message = ex.message ?: ""
            }
            (ex is ClientAccessDeniedException || code == ErrorCode.ACCESS_DENIED) -> {
                httpStatus = HttpStatus.FORBIDDEN
                message = ex.message ?: ""
            }
            (ex is BaseException) -> {
                httpStatus = HttpStatus.BAD_REQUEST
                code = ex.code
                message = ex.message
            }
            (ex is ResponseStatusException) -> {
                httpStatus = ex.status
                message = ex.message
            }
            else -> {
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR
                message = "Internal Server Error"
            }
        }

        // Encapsulate the response body, this body can be modified to its own jsonBody
        val result: MutableMap<String, Any> = HashMap(2, 1F)
        result["httpStatus"] = httpStatus
        val msg = "{\"code\": \"${if (code.isNotBlank()) code else httpStatus}\", \"message\": \"$message\"}"
        result["body"] = msg

        // error record
        val request = exchange.request
        logger.error("[Global Exception Handling] exception request path: {}, record exception information: {}", request.path, ex.message)

        // Reference AbstractErrorWebExceptionHandler
        if (exchange.response.isCommitted) {
            return Mono.error(ex)
        }
        exceptionHandlerResult.set(result)
        val newRequest = ServerRequest.create(exchange, messageReaders)

        return RouterFunctions.route(RequestPredicates.all(), HandlerFunction { request: ServerRequest -> renderErrorResponse(request) }).route(newRequest)
                .switchIfEmpty(Mono.error(ex))
                .flatMap { handler: HandlerFunction<ServerResponse> -> handler.handle(newRequest) }
                .flatMap { response: ServerResponse -> write(exchange, response) }
    }

    /**
     * Refer to DefaultErrorWebExceptionHandler
     */
    private fun renderErrorResponse(request: ServerRequest): Mono<ServerResponse> {
        val result = exceptionHandlerResult.get()
        return ServerResponse.status((result["httpStatus"] as HttpStatus))
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(result["body"] ?: ""))
    }

    /**
     * Reference AbstractErrorWebExceptionHandler
     */
    private fun write(exchange: ServerWebExchange,
                      response: ServerResponse): Mono<out Void> {
        exchange.response.headers.contentType = response.headers().contentType
        return response.writeTo(exchange, ResponseContext())
    }

    /**
     * Reference AbstractErrorWebExceptionHandler
     */
    private inner class ResponseContext : ServerResponse.Context {
        override fun messageWriters(): List<HttpMessageWriter<*>> {
            return messageWriters
        }

        override fun viewResolvers(): List<ViewResolver?> {
            return viewResolvers
        }
    }
}
