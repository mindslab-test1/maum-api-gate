package ai.maum.api.infra.property

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class ExternalProfileProperties(
        @Value("\${profile:@null}")
        private val profile: String
) {
    val externalProfile: String
        get() = profile
}
