package ai.maum.api.infra.property

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration

@Configuration
class ExternalURIProperties(
    @Value("\${target.grpc.stats:@null}")
    val statsServerUri: String,
    @Value("\${target.api.auth:@null}")
    val authServerUri: String,
    @Value("\${routes.tts.uri:@null}")
    val ttsServerUri: String,
    @Value("\${routes.stt.uri:@null}")
    val sttServerUri: String,
    @Value("\${routes.lipsync.uri:@null}")
    val lipsyncServerUri: String
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    private fun externalUriMap(): Map<String, String> {
        return mapOf(
            "target.grpc.stats" to statsServerUri,
            "target.api.auth" to authServerUri,
            "routes.tts.uri" to ttsServerUri,
            "routes.stt.uri" to sttServerUri,
            "routes.lipsync.uri" to lipsyncServerUri
        )
    }

    init {
        logger.info(externalUriMap().toString())
    }

    fun externalUriList(): List<Map<String, String>> {
        return listOf(externalUriMap())
    }
}
