package ai.maum.api.boundaries

import ai.maum.api.infra.property.ExternalURIProperties
import org.slf4j.LoggerFactory
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.PredicateSpec
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AuthRouter(
    private val externalURIProperties: ExternalURIProperties
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Bean
    fun authProxyRouting(builder: RouteLocatorBuilder): RouteLocator {
        return builder.routes()
            .route { r: PredicateSpec -> r.path("/auth/**").uri(externalURIProperties.authServerUri) }
            .build()
    }
}
