package ai.maum.api.boundaries.util

import ai.maum.api.core.exception.ClientUnAuthorizedException
import ai.maum.api.infra.property.ExternalURIProperties
import ai.maum.api.utils.AuthorizationParser
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/admin")
class ExternalURIController (
    @Value("\${admin.key}")
    private val adminKey: String,
    @Value("\${admin.secret}")
    private val adminSecret: String,
    private val externalURIProperties: ExternalURIProperties
) {

    @GetMapping("/gateway/external-uris")
    fun list(@RequestHeader httpHeaders: HttpHeaders): ResponseEntity<Map<String, List<Map<String, String>>>> {
        val requestAdminKey = AuthorizationParser.parseDecodedValue(httpHeaders).first
        if (requestAdminKey != adminKey) {
            throw ClientUnAuthorizedException("Invalid admin-key($requestAdminKey). Please ask the admin.")
        }
        val requestAdminSecret = AuthorizationParser.parseDecodedValue(httpHeaders).second
        if (requestAdminSecret != adminSecret) {
            throw ClientUnAuthorizedException("Invalid admin-secret($requestAdminSecret). Please ask the admin.")
        }
        val externalUriList = externalURIProperties.externalUriList()

        return ResponseEntity(mapOf("uris" to externalUriList), HttpStatus.OK)
    }
}
