package ai.maum.api.boundaries.util

import ai.maum.api.infra.property.ExternalProfileProperties
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class ProfileController(
    private val externalProfileProperties: ExternalProfileProperties
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @GetMapping("/gateway/profile")
    fun profile(): String {
        val externalProfile = externalProfileProperties.externalProfile
        logger.info("External profile: $externalProfile")
        return externalProfile
    }
}
