package ai.maum.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
class MaumApiGateApplication

fun main(args: Array<String>) {
	runApplication<MaumApiGateApplication>(*args)
}
