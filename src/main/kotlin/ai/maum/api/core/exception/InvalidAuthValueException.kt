package ai.maum.api.core.exception

import ai.maum.api.utils.ErrorCode

class InvalidAuthValueException(
        override val message: String
) : BaseException(ErrorCode.INVALID_AUTH_VALUE, message)
