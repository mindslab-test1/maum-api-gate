package ai.maum.api.core.exception

import ai.maum.api.utils.ErrorCode

class FailedToGetAccessTokenException(
        override val message: String
) : BaseException(ErrorCode.FAILED_TO_GET_ACCESS_TOKEN, message)
