package ai.maum.api.core.exception

import ai.maum.api.utils.ErrorCode

class ClientUnAuthorizedException(
        override val message: String
) : BaseException(ErrorCode.UNAUTHORIZED, message)
