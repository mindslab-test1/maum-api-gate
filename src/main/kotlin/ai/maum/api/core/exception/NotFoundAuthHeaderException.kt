package ai.maum.api.core.exception

import ai.maum.api.utils.ErrorCode

class NotFoundAuthHeaderException(
        override val message: String
) : BaseException(ErrorCode.NOT_FOUND_AUTH_HEADER, message)
