package ai.maum.api.core.exception

import java.lang.RuntimeException

open class BaseException(
        val code: String,
        override val message: String
) : RuntimeException(message)