package ai.maum.api.core.exception

import ai.maum.api.utils.ErrorCode

class ClientAccessDeniedException(
        override val message: String
) : BaseException(ErrorCode.ACCESS_DENIED, message)
