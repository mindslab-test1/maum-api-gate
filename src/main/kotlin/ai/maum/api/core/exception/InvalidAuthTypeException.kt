package ai.maum.api.core.exception

import ai.maum.api.utils.ErrorCode

class InvalidAuthTypeException(
        override val message: String
) : BaseException(ErrorCode.INVALID_AUTH_TYPE, message)
