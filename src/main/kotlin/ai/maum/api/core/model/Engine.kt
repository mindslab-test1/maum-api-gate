package ai.maum.api.core.model

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonValue
import org.springframework.http.server.RequestPath

enum class Engine(@get:JsonValue val value: String) {
    STT("stt"),
    TTS("tts"),
    LIPSYNC("lipsync");

    companion object {
        private val map = values().associateBy(Engine::value)

        @JsonCreator
        @JvmStatic
        fun fromValue(value: String) = map.getOrDefault(value, TTS)

        @JvmStatic
        fun findByRequestPath(requestPath: RequestPath): String {
            return values().find {
                requestPath.toString().contains(it.value)
            }?.value ?: return requestPath.elements()[1].value()
        }
    }
}